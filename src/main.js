import { createApp } from 'vue'
import App from './App.vue'
import "./styles/main.scss";
import Toast, {POSITION } from "vue-toastification";
import "vue-toastification/dist/index.css";

createApp(App)
.use(Toast, { position: POSITION.BOTTOM_RIGHT,toastClassName:'above-menu-bar toast-background-pink-1' })
.mount('#app')
